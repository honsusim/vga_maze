# Akcelerometrem ovládané VGA bludiště

Semestrální práce pro předmět *Návrh systémů VLSI*, ČVUT FEL, 2024.

Jedná se o jednoduchou hru v podobě bludiště, které hráč prochází pomocí míčku. Hra se zobrazuje pomocí VGA na monitor a pohyb míčku je ovládán pomocí akcelerometru. Projekt byl realizován pomocí jazyka VHDL s využitím prostředí Vivado a desky Nexys 4 s FPGA značky Xilinx.

Součástí repozitáře jsou zdrojové soubory a zpráva blíže popisující projekt.

Demonstrační video: https://youtu.be/iFhnT7DSPTY

## Princip hry
Cílem hry je nakláněním desky přesunou míček ze zeleného startu do modrého cíle. Míček se může pohybovat horizontálně, vertikálně a diagonálně konstantní rychlostí.

V momentě, kdy se míček ocitne v cíli, se zobrazí nápis oznamující hráči vítězství.

<p align="middle">
  <img src="/images/maze.png" height="400" />
  <img src="/images/win.png" height="400" />
</p>

