library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is
end top_tb;

architecture behavioral of top_tb is

    signal CLK_100MHZ : std_logic := '1';
    signal CPU_RST    : std_logic := '0';   
    
    signal clk : std_logic;
    signal rst : std_logic;
     
    -- VGA signaly barev
    signal VGA_R : std_logic;
    signal VGA_G : std_logic;
    signal VGA_B : std_logic;
    
    -- VGA synchronizacni signaly
    signal VGA_HS : std_logic;
    signal VGA_VS : std_logic;
    
    -- signaly horizontalni a vertikalni pozice VGA driveru
    signal vga_x : integer range 0 to 1040;
    signal vga_y : integer range 0 to 666;
    
    -- vystupu barevneho muxu
    signal col_r, col_g, col_b : std_logic;
    
    -- polomer, pozice a vykresleni micku
    constant ball_rad : integer range 0 to 800 := 1;
    signal ball_x : integer range 0 to 799;
    signal ball_y : integer range 0 to 599;    
    signal ball_r, ball_g, ball_b : std_logic := '0';
    
    -- vykresleni bludiste
    signal maze_r, maze_g, maze_b : std_logic;
    
    -- vykresleni napisu
    signal win_r, win_g, win_b : std_logic;
    
    -- zrychleni <0;511> ~ <-1g;1g>
    -- simulovany vystup z akcelerometru
    signal accel_x : std_logic_vector(8 downto 0);
    signal accel_y : std_logic_vector(8 downto 0);
    
    -- signaly pro pristup do pameti
    -- pro vykresleni bludiste a reseni kolizi micku
    signal maze_data_a : std_logic_vector(0 downto 0);
    signal maze_addr_a : std_logic_vector(18 downto 0);
    signal maze_data_b : std_logic_vector(0 downto 0);
    signal maze_addr_b : std_logic_vector(18 downto 0);    
    signal win_data : std_logic_vector(0 downto 0);
    signal win_addr : std_logic_vector(18 downto 0);  

begin

    rst <= not CPU_RST;
    
    clk_div_i : entity work.clk_div
    port map (
        clk_100mhz => CLK_100MHZ,
        
        clk_50mhz => clk
    );
    
    vga_drv_i : entity work.vga_drv
    port map (
        clk => clk,
        rst => rst,
        col_in_r => col_r,
        col_in_g => col_g,
        col_in_b => col_b,
        
        vga_x => vga_x,      
        vga_y => vga_y,        
        vga_hs => VGA_HS,
        vga_vs => VGA_VS,
        vga_r => VGA_R,
        vga_g => VGA_G,
        vga_b => VGA_B
    );
    
    vga_col_mux_i : entity work.vga_col_mux
    port map (
        clk => clk,
        rst => rst,        
        ball_r => ball_r,
        ball_g => ball_g,
        ball_b => ball_b,    
        maze_r => maze_r,
        maze_g => maze_g,
        maze_b => maze_b,
        win_r => win_r,
        win_g => win_g,
        win_b => win_b,
        
        col_out_r => col_r,
        col_out_g => col_g,
        col_out_b => col_b
    );
    
    maze_i : entity work.maze
    port map (
        clk => clk,
        rst => rst,
        vga_x => vga_x,
        vga_y => vga_y,
        rom_data => maze_data_a(0),
        
        rom_addr => maze_addr_a,
        col_out_r => maze_r,
        col_out_g => maze_g,
        col_out_b => maze_b
    ); 
    
    maze_rom_i : entity work.maze_rom
    port map (
        clka => clk,
        addra => maze_addr_a,
        douta => maze_data_a,
        clkb => clk,
        addrb => maze_addr_b,
        doutb => maze_data_b
    );
    
    win_i : entity work.win
    port map (
        clk => clk,
        rst => rst,
        vga_x => vga_x,
        vga_y => vga_y,
        ball_x => ball_x,
        ball_y => ball_y,
        rom_data => win_data(0),
        
        rom_addr => win_addr,
        col_out_r => win_r,
        col_out_g => win_g,
        col_out_b => win_b
    );     
    
    win_rom_i : entity work.win_rom
    port map (
        clka => clk,
        addra => win_addr,
        douta => win_data        
    ); 
    
    ball_i : entity work.ball
    port map (
        clk => clk,
        rst => rst,
        vga_x => vga_x,
        vga_y => vga_y,
        ball_x => ball_x,
        ball_y => ball_y,
        radius => ball_rad,
        
        col_out_r => ball_r,
        col_out_g => ball_g,
        col_out_b => ball_b
    );
    
    ball_move_i : entity work.ball_move
    port map (
        clk => clk,
        rst => rst,
        radius => ball_rad,
        accel_x => accel_x,
        accel_y => accel_y,
        rom_data => maze_data_b(0),
        
        rom_addr => maze_addr_b,
        ball_x => ball_x,
        ball_y => ball_y
    );
    
    --------------------------------------------------------------------------
    
    CLK_100MHZ <= not CLK_100MHZ after 5 ns;
    
    testbench : process
    begin
        accel_x <= std_logic_vector(to_unsigned(255,accel_x'length)); -- 0
        accel_y <= std_logic_vector(to_unsigned(255,accel_x'length)); -- 0
    
        wait for 100 ns;
        CPU_RST <= '1';
        
        -- simulace naklonu
        accel_x <= std_logic_vector(to_unsigned(511,accel_x'length)); -- 256
        accel_y <= std_logic_vector(to_unsigned(511,accel_y'length)); -- 256
        
        wait for 20 ms;
        wait;
    end process testbench;

end behavioral;
