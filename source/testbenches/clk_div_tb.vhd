library ieee;
use ieee.std_logic_1164.all;

entity clk_div_tb is
end clk_div_tb;

architecture behavioral of clk_div_tb is

    signal clk_100mhz : std_logic := '1';
    signal clk_50mhz : std_logic;

begin

    clk_div_i : entity work.clk_div
    port map (
        clk_100mhz => clk_100mhz,        
        clk_50mhz => clk_50mhz
    );
    
    clk_100mhz <= not clk_100mhz after 5 ns;

end behavioral;
