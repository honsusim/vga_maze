library ieee;
use ieee.std_logic_1164.all;

entity vga_col_mux_tb is
end vga_col_mux_tb;

architecture behavioral of vga_col_mux_tb is
    
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';
    
    -- vstupni signaly
    signal ball_r, ball_g, ball_b : std_logic := '0';
    signal maze_r, maze_g, maze_b : std_logic := '0';
    signal win_r, win_g, win_b : std_logic := '0';
    
    -- vystupni signaly
    signal col_r, col_g, col_b : std_logic;

begin

    vga_col_mux_i : entity work.vga_col_mux
    port map (
        clk => clk,
        rst => rst,        
        ball_r => ball_r,
        ball_g => ball_g,
        ball_b => ball_b,    
        maze_r => maze_r,
        maze_g => maze_g,
        maze_b => maze_b,
        win_r => win_r,
        win_g => win_g,
        win_b => win_b,
        
        col_out_r => col_r,
        col_out_g => col_g,
        col_out_b => col_b
    );
    
    clk <= not clk after 10 ns;
    
    testbench: process
    begin
        maze_r <= '1';
        
        wait for 100 ns;
        rst <= '0';
        
        wait for 100 ns;
        ball_g <= '1';
        
        wait for 100 ns;
        win_b <= '1';
        
        wait;
    
    end process testbench;

end behavioral;
