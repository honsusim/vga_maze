library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity vga_drv is
    port ( 
        clk : in std_logic;
        rst : in std_logic;
        
        -- vstup barev z RGB muxu
        col_in_r : in std_logic;
        col_in_b : in std_logic;
        col_in_g : in std_logic;
        
        -- signaly horizontalni a vertikalni pozice VGA driveru
        vga_x : out integer range 0 to 1040;      
        vga_y : out integer range 0 to 666;
        
        -- vystupni signaly barev a synchronizace          
        vga_hs : out std_logic;
        vga_vs : out std_logic;
        vga_r  : out std_logic;
        vga_g  : out std_logic;
        vga_b  : out std_logic
    );
end vga_drv;

architecture behavioral of vga_drv is
 
  -- rezim 800 x 600 x 72 Hz
  constant vga_hs_MAX : integer := 1040; -- celkova delka HS pulzu
  constant vga_vs_MAX : integer := 666;  -- celkova delka VS pulzu
  
  -- konstanty casovani
  constant vga_hs_SYNC_STOP : integer := 120;
  constant vga_hs_VON_START : integer := 120+64;
  constant vga_hs_VON_STOP  : integer := 120+64+800;
  constant vga_vs_SYNC_STOP : integer := 6;
  constant vga_vs_VON_START : integer := 6+23;
  constant vga_vs_VON_STOP  : integer := 6+23+600;
  
  -- celkove citace - pro synchronizaci jednotlivych bloku vykresleni
  signal vga_hs_count : integer range 0 to vga_hs_MAX; -- pro HS
  signal vga_vs_count : integer range 0 to vga_vs_MAX; -- pro VS
  
  -- interni signaly pro pozici - pro kontrolu synchronizace pri simulaci
  signal vga_x_pos_i : integer range 0 to 800;
  signal vga_y_pos_i : integer range 0 to 600;
  
  signal VIDEO_ON : std_logic;
  	
begin

    -- prirazeni internich signalu 
	vga_x <= vga_hs_count;	
	vga_y <= vga_vs_count;

    -- generovani synchronizacnich signalu
	VSHS_gen : process(clk, rst)
    begin
		if rising_edge(clk) then 
			if rst = '1' then
				vga_hs_count <= 0;
				vga_vs_count <= 0;
				vga_x_pos_i  <= 0;
				vga_y_pos_i  <= 0; -- puvodne 600
			else
                if vga_hs_count < vga_hs_MAX then 
                    vga_hs_count <= vga_hs_count + 1;                  
                    if VIDEO_ON = '1' then -- pozice se aktualizuje jen pokud se vykresluje
                        vga_x_pos_i <= vga_x_pos_i + 1;
                    end if;
                else -- preteceni HS citace
                    vga_hs_count <= 0;
                    vga_x_pos_i  <= 0;				  
                    if vga_vs_count < vga_vs_MAX then
                        vga_vs_count <= vga_vs_COUNT + 1;                    
                        if vga_vs_count > vga_vs_VON_START and vga_vs_count < vga_vs_VON_STOP then -- pozice se aktualizuje jen pokud se vykresluje
                            vga_y_pos_i <= vga_y_pos_i + 1; -- puvodne minus
                        end if;
                    else  -- preteceni VS citace -> novy snimek
                        vga_vs_count <= 0;
                        vga_y_pos_i  <= 0; -- puvodne 600
                    end if; 
           		end if;  
			 end if;
		end if;	
	end process VSHS_gen;	

    -- generovani vystupnich signalu (R, G, B, VS, HS)
    output : process(rst, clk)
    begin
        if rising_edge(clk) then   
            if rst = '1' then
                vga_r  <= '0';
                vga_g  <= '0';
                vga_b  <= '0';
                VIDEO_ON <= '0';
                vga_hs <= '0';
                vga_vs <= '0';	
            else	 
                -- generovani HS
                if vga_hs_count < vga_hs_SYNC_STOP then 
                    vga_hs <= '0';
                else 
                    vga_hs <= '1'; 
                end if;
        
                -- generovani VS
                if vga_vs_count < vga_vs_SYNC_STOP then
                    vga_vs <= '0'; 
                else 
                    vga_vs <= '1';
                end if;
        
                -- video on
                if vga_hs_count > vga_hs_VON_START and vga_hs_count < (vga_hs_VON_STOP + 1) and
                   vga_vs_count > vga_vs_VON_START and vga_vs_count < (vga_vs_VON_STOP + 1) then
                    VIDEO_ON <= '1';
                    vga_r <=  col_in_r; 
                    vga_g <=  col_in_g; 
                    vga_b <=  col_in_b;
                else -- zatemneni
                    VIDEO_ON <= '0';
                    vga_r <= '0';
                    vga_g <= '0';
                    vga_b <= '0';
                end if;        
            end if;
        end if;
    end process output;    

end behavioral;