library ieee;
use ieee.std_logic_1164.all;

entity clk_div is
    port ( 
        clk_100mhz : in std_logic;
        clk_50mhz  : out std_logic
    );
end clk_div;

architecture behavioral of clk_div is

    signal clk_i : std_logic := '0';

begin

    clk_50mhz <= clk_i;
    
    div : process(clk_100mhz)
    begin
        if rising_edge(clk_100mhz) then
            clk_i <= not clk_i;
        end if;
    end process div;

end behavioral;
