library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ball_move is
    port (
        clk : in std_logic;
        rst : in std_logic;
        
        -- polomer kruhu
        radius : in integer;
        
        -- zrychleni (naklon) z akcelerometru
        -- 0 ~ -1g, 255 ~ 0g, 511 ~ 1g
        accel_x : in std_logic_vector(8 downto 0);
        accel_y : in std_logic_vector(8 downto 0);  
        
        -- pristup k bludisti ulozenem v ROM        
        rom_data : in std_logic;
        rom_addr : out std_logic_vector(18 downto 0);
        
        -- aktualni pozice stredu micku
        ball_x : out integer range 0 to 799;
        ball_y : out integer range 0 to 599
             
    );
end ball_move;

architecture behavioral of ball_move is

    -- signaly pro delicku
    constant div_max : integer := 1040*333; -- odpovida 144 Hz
    signal div : integer range 0 to div_max := 0;
    
    -- signaly pro pohyb micku
    signal ball_x_i : integer range 0 to 799;
    signal ball_y_i : integer range 0 to 599;
    signal a_x, a_y : integer range -255 to 256;
    constant a_thr : integer := 12;
    
    -- signaly pro detekci kolize
    signal coll_x, coll_y : integer range -1 to 1 := 0; -- nastala kolize ve smeru x nebo y
    signal cnt_rom : integer := 0;                      -- citac pro NACTENI pixelu na jedne strane micku
    signal cnt_rom_side : integer range 0 to 3 := 0;    -- citac pro NACTENI stran micku
    signal cnt : integer := 0;                          -- citac pro KONTROLU pixelu na jedne strane micku
    signal cnt_side : integer range 0 to 3 := 0;        -- citac pro KONTROLU stran micku   

begin

    ball_x <= ball_x_i;
    ball_y <= ball_y_i;
    
    -- skalovani naklonu na <-255;256>
    a_x <= to_integer(unsigned(accel_x)) - 255;
    a_y <= to_integer(unsigned(accel_y)) - 255;

    -- delicka pro zpomaleni pohybu micku
    divider : process(clk, rst)
    begin
        if rising_edge(clk) then
            if rst = '1' or div = div_max then
                div <= 0;
            else
                div <= div + 1;
            end if;
        end if;
    end process divider;
    
    -- citac pro nacitani pixelu okolo micku
    rom_count : process(clk,rst)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                cnt_rom <= 0;
                cnt_rom_side <= 0;
            else
                -- citani vsech pixelu okolo micku s vyjimkou rohovych
                if div < 4*(2*radius - 1) then
                    -- citani pixelu na jedne strane micku <0 - (2*radius - 2)>
                    if cnt_rom < (2*radius - 2) then
                        cnt_rom <= cnt_rom + 1;
                    else                        
                        cnt_rom <= 0;
                        -- citani jednotlivych stran micku <0 - 3>
                        if cnt_rom_side = 3 then
                            cnt_rom_side <= 0;    
                        else
                            cnt_rom_side <= cnt_rom_side + 1;
                        end if;
                    end if;        
                end if;         
            end if;
        end if;        
    end process rom_count;
    
    -- citac pro kontrolu kolize
    -- o 2 clk zpozdeny pro kompenzaci zpozdeni cteni z pameti
    collision_count : process(clk,rst)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                cnt <= 0;
                cnt_side <= 0;
            else
                -- citani vsech pixelu okolo micku s vyjimkou rohovych
                if div > 1 and div < (4*(2*radius - 1) + 2) then
                    -- citani pixelu na jedne strane micku <0 - (2*radius - 2)>
                    if cnt < (2*radius - 2) then
                        cnt <= cnt + 1;
                    else                        
                        cnt <= 0;
                        -- citani jednotlivych stran micku <0 - 3>
                        if cnt_side = 3 then
                            cnt_side <= 0;    
                        else
                            cnt_side <= cnt_side + 1;
                        end if;
                    end if;        
                end if;         
            end if;
        end if;        
    end process collision_count;
    
    -- nacteni pixelu okolo micku z pameti
    rom_read : process(clk)
    begin
        if rising_edge(clk) then
            if cnt_rom_side = 0 then 
                -- nacteni horni strany
                rom_addr <= std_logic_vector(to_unsigned((ball_x_i - radius + cnt_rom + 1) + 800*(ball_y_i - radius), rom_addr'length));                     
            elsif cnt_rom_side = 1 then 
                -- nacteni leve strany
                rom_addr <= std_logic_vector(to_unsigned((ball_x_i - radius) + 800*(ball_y_i - radius + cnt_rom + 1), rom_addr'length));                
            elsif cnt_rom_side = 2 then
                -- nacteni dolni strany
                rom_addr <= std_logic_vector(to_unsigned((ball_x_i - radius + cnt_rom + 1) + 800*(ball_y_i + radius), rom_addr'length)); 
            elsif cnt_rom_side = 3 then
                -- nacteni prave strany
                rom_addr <= std_logic_vector(to_unsigned((ball_x_i + radius) + 800*(ball_y_i - radius + cnt_rom + 1), rom_addr'length)); 
            end if;
        end if;    
    end process rom_read;
    
    -- detekce kolize
    collision : process(clk, rst)
    begin
        if rising_edge(clk) then
            if rst = '1' or div = div_max then
                coll_x <= 0;
                coll_y <= 0;
            else
                if cnt_side = 0 then 
                    -- kontrola horni strany
                    if rom_data = '1' then 
                        coll_y <= -1;  
                    end if;               
                elsif cnt_side = 1 then 
                    -- kontrola leve strany
                    if rom_data = '1' then 
                        coll_x <= -1;  
                    end if;               
                elsif cnt_side = 2 then
                    -- kontrola dolni strany
                    if rom_data = '1' then 
                        coll_y <= 1;  
                    end if;
                elsif cnt_side = 3 then
                    -- kontrola prave strany
                    if rom_data = '1' then 
                        coll_x <= 1;  
                    end if; 
                end if;
            end if;
        end if;
    end process collision;
    
    -- pohyb micku
    move : process(clk, rst)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                ball_x_i <= 40;
                ball_y_i <= 40;
                
            elsif div = div_max then
            
                -- horizontalni kolize
                if coll_x = -1 then -- kolize levou hranou
                    if a_x > a_thr then -- jen pohyb doprava
                        ball_x_i <= ball_x_i + 1; 
                    end if;             
                elsif coll_x = 1 then -- kolize pravou hranou
                    if a_x < -a_thr then -- jen pohyb doleva
                        ball_x_i <= ball_x_i - 1; 
                    end if; 
                else -- bez kolize
                    if a_x > a_thr then
                        ball_x_i <= ball_x_i + 1;
                    elsif a_x < -a_thr then
                        ball_x_i <= ball_x_i - 1;
                    end if;       
                end if;
                       
                -- vertikalni kolize                      
                if coll_y = -1 then -- kolize horni hranou
                    if a_y > a_thr then -- jen pohyb dolu          
                        ball_y_i <= ball_y_i + 1;
                    end if;                      
                elsif coll_y = 1 then -- kolize spodni hranou
                    if a_y < -a_thr then -- jen pohyb nahoru         
                        ball_y_i <= ball_y_i - 1;
                    end if;                      
                else -- bez kolize
                    if a_y > a_thr then
                        ball_y_i <= ball_y_i + 1;
                    elsif a_y < -a_thr then
                        ball_y_i <= ball_y_i - 1;
                    end if;           
                end if;
                
            end if;
        end if;
    end process move;

end behavioral;


