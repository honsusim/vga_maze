library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity win is
    port (
        clk  : in std_logic;
        rst  : in std_logic;
        
        -- signaly horizontalni a vertikalni pozice VGA driveru
        vga_x : in integer range 0 to 1040;
        vga_y : in integer range 0 to 666;
        
        -- aktualni pozice stredu micku
        ball_x : in integer range 0 to 799;
        ball_y : in integer range 0 to 599;
        
        -- pristup k napisu ulozenem v ROM        
        rom_data : in std_logic;
        rom_addr : out std_logic_vector(18 downto 0);
        
        -- vystupni barvy
        col_out_r : out std_logic;
        col_out_g : out std_logic;
        col_out_b : out std_logic
    );
end win;

architecture behavioral of win is
    
    constant delay : integer := 5;
    
    -- interni signaly pro pozici na displeji
    signal vga_x_i : integer range 0 to 799;
    signal vga_y_i : integer range 0 to 599;
    
    -- konstanty casovani VGA
    constant vga_hs_VON_START : integer := 120+64;
    constant vga_hs_VON_STOP  : integer := 120+64+800;
    constant vga_vs_VON_START : integer := 6+23;
    constant vga_vs_VON_STOP  : integer := 6+23+600;
    
    signal won : std_logic := '0';

begin

    -- cteni napisu z pameti a zobrazeni
    text : process(clk, rst)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                col_out_r <= '0';
                col_out_g <= '0';
                col_out_b <= '0';
            else                
                -- nastaveni adresy ROM k precteni (0 - 479999)
                rom_addr <= std_logic_vector(to_unsigned(vga_x_i + 800*vga_y_i, rom_addr'length));
                                    
                -- zobrazeni napisu
                if won = '1' and rom_data = '1' then
                    col_out_r <= '1';
                    col_out_g <= '1';
                    col_out_b <= '0';
                else                    
                    col_out_r <= '0';
                    col_out_g <= '0';
                    col_out_b <= '0';
                end if;      
            end if;
        end if;        
    end process text;
    
    -- vyhodnoceni zda uzivatel vyhral na zaklade pozice micku
    win : process(clk, rst)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                won <= '0';
            else
                if ball_x > 750 and ball_y > 550 then
                    won <= '1';
                end if;
            end if;
        end if;
    end process win;
    
    -- vnitrni logika pro prednacteni napisu a eliminaci zpozdeni pro spravne vykresleni
    preload : process(clk, rst)
        variable tmp : integer;
    begin
        if rising_edge(clk) then        
            -- VGA vykresluje v intervalu vga_x <186;985> ~ <0;799> na displeji
            if vga_x > (vga_hs_VON_START - delay) and vga_x < (vga_hs_VON_STOP + 1 - delay) then
                tmp := vga_x - vga_hs_VON_START - 1 + delay;            
                if tmp > 799 then
                    tmp := tmp - 800;
                end if;
                vga_x_i <= tmp;
            else
                vga_x_i <= 0;
            end if;
            
            -- VGA vykresluje v intervalu vga_y <30;629> ~ <0;599> na displeji
            if vga_y > vga_vs_VON_START and vga_y < (vga_vs_VON_STOP + 1) then
                vga_y_i <= vga_y - vga_vs_VON_START - 1;
            else
                vga_y_i <= 0;
            end if;                  
        end if; 
    end process preload;

end behavioral;
