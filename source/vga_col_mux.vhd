library ieee;
use ieee.std_logic_1164.all;

entity vga_col_mux is
    port (
        clk : in std_logic;
        rst : in std_logic;
              
        ball_r : in std_logic;
        ball_g : in std_logic;
        ball_b : in std_logic;
    
        maze_r : in std_logic;
        maze_g : in std_logic;
        maze_b : in std_logic;
        
        win_r : in std_logic;
        win_g : in std_logic;
        win_b : in std_logic;
        
        col_out_r : out std_logic;
        col_out_g : out std_logic;
        col_out_b : out std_logic
    );
end vga_col_mux;

architecture behavioral of vga_col_mux is

begin

    process(clk, rst)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                col_out_r <= '0';
                col_out_g <= '0';
                col_out_b <= '0';
            else
                -- priorita zobrazeni: vitezny napis > micek > bludiste
                if win_r = '1' or win_g = '1' or win_b = '1' then
                    col_out_r <= win_r;
                    col_out_g <= win_g;
                    col_out_b <= win_b;                                    
                elsif ball_r = '1' or ball_g = '1' or ball_b = '1' then
                    col_out_r <= ball_r;
                    col_out_g <= ball_g;
                    col_out_b <= ball_b;
                else
                    col_out_r <= maze_r;
                    col_out_g <= maze_g;
                    col_out_b <= maze_b;                    
                end if;                   
            end if;            
        end if;
    end process;

end behavioral;
